package main

import (
	"github.com/go-martini/martini"
	"github.com/martini-contrib/cors"
	"github.com/martini-contrib/render"
)

type FoodMap struct {
	Id         int            `json:"id"`
	Data       map[string]int `json:"data"`
	SearchTerm string         `json:"searchTerm"`
}

func main() {
	m := martini.Classic()
	m.Use(render.Renderer())
	m.Use(cors.Allow(&cors.Options{
		AllowOrigins:     []string{"*", "http://development.food-us.divshot.io", "http://staging.food-us.divshot.io", "http://food-us.divshot.io"},
		AllowMethods:     []string{"GET", "POST"},
		AllowHeaders:     []string{"Origin"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
	}))

	m.Get("/maps/:id", func(params martini.Params, r render.Render) {
		foodMap := &FoodMap{
			Id:         1,
			Data:       map[string]int{"AK": 0, "AL": 90, "AR": 26, "AZ": 3, "CA": 67, "CO": 17, "CT": 8, "DE": 2, "FL": 66, "GA": 100, "HI": 0, "IA": 1, "ID": 0, "IL": 40, "IN": 100, "KS": 0, "KY": 38, "LA": 32, "MA": 8, "MD": 96, "ME": 2, "MI": 20, "MN": 7, "MO": 4, "MS": 46, "MT": 0, "NC": 100, "ND": 0, "NE": 1, "NH": 0, "NJ": 1, "NM": 1, "NV": 0, "NY": 95, "OH": 11, "OK": 22, "OR": 100, "PA": 16, "RI": 0, "SC": 100, "SD": 0, "TN": 100, "TX": 100, "UT": 5, "VA": 100, "VT": 3, "WA": 11, "WI": 29, "WV": 2, "WY": 3},
			SearchTerm: "Something Else"}

		r.JSON(200, foodMap)
	})
	m.Run()
}
